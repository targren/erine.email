<div class="content-parent">
  <div class="content-child-middle">
    <div class="container marketing">

      <!-- Featurette -->
      <div class="row featurette">
        <div class="col-md-7 col-md-push-5">
          <p class="lead">Hi! I'm Mikaël Davranche. Strong believer in Open Source, I love building and running massive-scale production systems, and making sure they can grow sustainably. I'm a lucky man, that's my job.</p>
          <p class="lead">I was a strong <a href="https://www.spamgourmet.com" target="_blank">Spamgourmet</a> user for years, before its discontinuation. I loved it. I created erine.email to be fully aware of the issues related to such a solution, as a challenge, as a game.</p>
        </div>
        <div class="col-md-5 col-md-pull-7">
          <img class="featurette-image img-responsive center-block" src="/static/images/profile.jpg" alt="Mikael Davranche" style='border-radius: 100%;'>
        </div>
      </div>

      <p class="lead" style="margin-top: 60px;">Feel free to contact me or keep in touch on Twitter:</p>
      <p><a class="btn btn-viewdetails" href="&#109;&#097;&#105;&#108;to:&#099;&#111;&#110;&#116;&#097;&#099;&#116;&#064;erine.email" role="button"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send me an email</a> <a class="btn btn-viewdetails" href="https://twitter.com/ErineEmail" target="_blank" role="button"><i class="fa fa-twitter" aria-hidden="true"></i> Follow me on Twitter</a></p>
      <p class="lead" style="margin-top: 60px;">I hope you'll enjoy erine.email.<img src="/static/images/mikael.png" alt="Mikael" style='margin-left:5%;'></p>
    </div>
    <p style="text-align: left;"><a href="https://unsplash.com/photos/VybzKEUMhbw">Home picture</a> by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@muukii?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Muukii"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Muukii</span></a></p>
  </div>
</div>
