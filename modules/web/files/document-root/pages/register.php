<?php
  if($user->isSigned()) redirect("/disposableMails");
?>
<div class="content-parent">
  <div class="content-child-top">
    <div class="container marketing">
      <form class="form-signin" method="post" action="actions/register.php" data-success="/registerOK">
        <h3 class="form-signin-heading">Get started now!</h3>
        <div class="form-group">
          <label for="username" class="sr-only">Username</label>
          <input name="username" type="text" class="form-control" placeholder="username" required autofocus>
        </div>
        <div class="form-group">
          <label for="firstName" class="sr-only">First name</label>
          <input name="firstName" type="text" class="form-control" placeholder="First name">
        </div>
        <div class="form-group">
          <label for="lastName" class="sr-only">Last name</label>
          <input name="lastName" type="text" class="form-control" placeholder="Last name">
        </div>
        <div class="form-group">
          <label for="mailAddress" class="sr-only">Email</label>
          <input name="mailAddress" type="text" class="form-control" placeholder="Email" required>
        </div>
        <div class="form-group">
          <label for="password" class="sr-only">Password</label>
          <input name="password" type="password" class="form-control" placeholder="Password" required>
        </div>
        <div class="form-group">
          <label for="password2" class="sr-only">Confirm password</label>
          <input name="password2" type="password" class="form-control" placeholder="Confirm password" required>
        </div>
        <div class="form-group text-center">
          <button type="submit" class="btn btn-primary btn-block">Register</button>
          <br>
          <a href="/login" class="">Login</a>
        </div>
      </form>
    </div>
  </div>
</div>
