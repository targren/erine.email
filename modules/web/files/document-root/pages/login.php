<?php
  if($user->isSigned()) redirect("/disposableMails");
?>
<div class="content-parent">
  <div class="content-child-top">
    <div class="container marketing">
      <form class="form-signin" method="post" action="actions/login.php" data-success="/disposableMails">
        <h3 class="form-signin-heading">Please sign in</h3>
        <div class="form-group">
          <label for="username" class="sr-only">Username</label>
          <input name="username" type="text" class="form-control" placeholder="Username" required autofocus>
        </div>
        <div class="form-group">
          <label for="password" class="sr-only">Password</label>
          <input name="password" type="password" class="form-control" placeholder="Password" required>
        </div>
        <div class="form-group text-center">
          <button name="SignIn" type="submit" class="btn btn-primary btn-block">Sign in</button>
          <br/>
          <a href="/register">Register a new account</a>
          <br>
          <a href="/resetPassword">Forgot password?</a>
        </div>
      </form>
    </div>
  </div>
</div>
