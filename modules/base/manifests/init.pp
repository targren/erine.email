# Install basic administration tools
class base
{

  package { [
    'bash-completion',
    'dnsutils',
    'less',
    'lsof',
    'mosh',
    'strace',
    'sysstat',
    'tree',
    'vim',
    'whois',
  ]:
    ensure => present,
  }

  $domainnames = hiera('domainnames')

  # bash configuration
  file { '/etc/bash.bashrc':
    ensure  => file,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => template('base/bash.bashrc.erb'),
  }

  # sysstat configuration
  file { '/etc/cron.d/sysstat':
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/base/sysstat.cron',
    # We need to overwrite the default file, not the other way around
    require => Package['sysstat'],
  }
  file { '/etc/default/sysstat':
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/base/sysstat',
    # We need to overwrite the default file, not the other way around
    require => Package['sysstat'],
  }

}
