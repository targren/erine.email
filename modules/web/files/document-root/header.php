<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="/static/images/favicon.png">

<?php
$title = "erine.email";
foreach ($menu as $key => $value)
{
  if (($pagePath == $value) || ($pagePath == "home" && $value == "/"))
  {
    $title = "erine.email / " . $key;
  }
}
print("<title>" . $title . "</title>");
?>

    <!-- Bootstrap core CSS -->
    <link href="/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <script src="https://kit.fontawesome.com/790de51d83.js" crossorigin="anonymous"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="/static/css/erine-email.css" rel="stylesheet">

    <!-- jQuery -->
    <script type="text/javascript" language="javascript" src="/bootstrap/assets/js/vendor/jquery.min.js"></script>

  </head>
<!-- NAVBAR
================================================== -->
  <body>
