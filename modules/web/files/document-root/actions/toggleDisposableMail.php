<?php
  include('../core/config.php');

  function returnResult($status)
  {
    $response_array['status'] = $status;
    header('Content-type: application/json');
    echo json_encode($response_array);
    exit();
  }

  if ((!$user->isSigned()) || (count($_POST) == 0))
  {
    returnResult('error');
  }
  $stmt = sqlquery($pdo, 'UPDATE disposableMailAddress SET enabled = :seten WHERE mailAddress = :ma AND enabled = :en AND userID = :ui LIMIT 1', ['seten' => ($_POST['presentStatus'] == 1) ? 0 : 1, 'ma' => $_POST['disposableMail'], 'en' => $_POST['presentStatus'], 'ui' => $user->ID]);
  if (($stmt) && ($stmt->rowCount() == 1))
  {
    returnResult('success');
  }
  returnResult('error');
?>
