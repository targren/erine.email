<?php

include('functions.php');
include('autoload.php');

$ini_array  = parse_ini_file("/etc/erine-email.conf");
$db_host    = $ini_array["host"];
$db_port    = $ini_array["port"];
$db_db      = $ini_array["database"];
$db_user    = 'www';
$db_pass    = rtrim(file_get_contents('/home/www-data/.mariadb.pwd'));
$db_charset = 'latin1';

//Instantiate the uFlex User object
$user = new \ptejada\uFlex\User();

//Add database credentials and information to uFlex User objets
$user->config->database->host = $db_host;
$user->config->database->port = $db_port;
$user->config->database->user = $db_user;
$user->config->database->password = $db_pass;
$user->config->database->name = $db_db;

/*
 * Instead of editing the Class files directly you may make
 * the changes in this space before calling the ->start() method.
 * For example: if we want to change the default username from "Guess"
 * to "Stranger" you do this:
 *
 * $user->config->userDefaultData->username = 'Stranger';
 *
 * You may change and customize all the options and configurations like
 * this, even the error messages. By exporting your customizations outside
 * the class file it will be easier to maintain your application configuration
 * and update the class core itself in the future.
 */

//Starts the object by triggering the constructor
$user->start();

//Second database connexion for everything but the user management
$dsn = "mysql:host=$db_host;port=$db_port;dbname=$db_db;charset=$db_charset";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo = new PDO($dsn, $db_user, $db_pass, $opt);

?>
