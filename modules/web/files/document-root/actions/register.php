<?php
include('../core/config.php');

//Add validation for custom fields, firstName and lastName

$user->addValidation(
    array(
        'firstName' => array(
            'limit' => '0-15',
            'regEx' => '/\w+/'
        ),
        'lastName'  => array(
            'limit' => '0-15',
            'regEx' => '/\w+/'
        ),
    )
);

/*
 * Alternative syntax to adding validation rules:
 *
 *      $user->addValidation('firstName','0-15','/\w+/');
 *      $user->addValidation('lastName','0-15','/\w+/');
 */

if (count($_POST))
{
  $input = new \ptejada\uFlex\Collection($_POST);

  /*
   * If the form fields names match your DB columns then you can reduce the collection
   * to only those expected fields using the filter() function
   */
  $input->filter('username', 'firstName', 'lastName', 'mailAddress', 'password', 'password2');

  $user->register($input, true);

  echo json_encode(
    array(
      'error'   => $user->log->getErrors(),
      'confirm' => 'Registration succeeded',
      'form'    => $user->log->getFormErrors(),
    )
  );
}
?>
